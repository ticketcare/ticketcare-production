/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$("#gotop").click(function () {
    jQuery("html,body").animate({
        scrollTop: 0
    }, 1000);
});
$(window).scroll(function () {
    if ($(this).scrollTop() > 300) {
        $('#gotop').fadeIn("fast");
    } else {
        $('#gotop').stop().fadeOut("fast");
    }
});