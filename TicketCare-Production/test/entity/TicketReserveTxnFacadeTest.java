/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class TicketReserveTxnFacadeTest {
    private static EJBContainer container;
    private static TicketDataFacade ticketDataFacade;
    private static ReceiverFacade receiverFacade;
    private static TicketReserveTxnFacade ticketReserveTxnFacade;
    private static TicketPickUpMethodFacade ticketPickUpMethodFacade;
    
    private List<Ticket> tickets;
    private List<Receiver> receivers;
    private List<TicketReserveTxn> ticketReserveTxns;
    private List<TicketPickUpMethod> pickupMethods;
    
    public TicketReserveTxnFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        container = EJBContainer.createEJBContainer();
        System.out.println("Opening the container");
        
        ticketDataFacade = (TicketDataFacade) container.getContext().lookup("java:global/classes/TicketDataFacade");
        receiverFacade = (ReceiverFacade) container.getContext().lookup("java:global/classes/ReceiverFacade");
        ticketReserveTxnFacade = (TicketReserveTxnFacade) container.getContext().lookup("java:global/classes/TicketReserveTxnFacade");
        ticketPickUpMethodFacade = (TicketPickUpMethodFacade) container.getContext().lookup("java:global/classes/TicketPickUpMethodFacade");
    }
    
    @AfterClass
    public static void tearDownClass() {
        container.close();
        System.out.println("Closing the container");
    }
    
    @Before
    public void setUp() {
        tickets = new ArrayList<>();
        receivers = new ArrayList<>();
        ticketReserveTxns = new ArrayList<>();
        pickupMethods = new ArrayList<>();
        
        //insert ticket data
        Ticket ticket1 = new Ticket();
        ticket1.setName("Ticket1");
        Calendar currentDate = Calendar.getInstance();
        ticket1.setCreate_date(currentDate.getTime());
        currentDate.add(Calendar.MONTH, 1); // due date: after one month.
        ticket1.setEndDate(currentDate.getTime());
        // add to collection
        ticketDataFacade.create(ticket1);
        tickets.add(ticket1);
        
        // insert receiver data
        Receiver receiver1 = new Receiver();
        receiver1.setName("Receiver Test 01");
        receiverFacade.create(receiver1);
        receivers.add(receiver1);
        // Create ticket cateogry
        TicketPickUpMethod pickupMethod_A = new TicketPickUpMethod();
        pickupMethod_A.setWay("A");
        ticketPickUpMethodFacade.create(pickupMethod_A);
        pickupMethods.add(pickupMethod_A);
        // insert reservation txn data
        TicketReserveTxn txn1 = new TicketReserveTxn();
        txn1.setReceiver_id(receiver1.getId());
        txn1.setTicket_id(ticket1.getId());
        currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DAY_OF_MONTH, 7);
        txn1.setDueDate(currentDate.getTime());
        txn1.setExecuteCode(1);
        txn1.setReserv_qty(2);
        
        txn1.setPickupMethod(pickupMethod_A);
        // persist txn 1
        ticketReserveTxnFacade.create(txn1);
        ticketReserveTxns.add(txn1);
        // txn 2
         TicketReserveTxn txn2 = new TicketReserveTxn();
        txn2.setReceiver_id(receiver1.getId());
        txn2.setTicket_id(ticket1.getId());
        currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DAY_OF_MONTH, 7);
        txn2.setDueDate(currentDate.getTime());
        txn2.setExecuteCode(1);
        txn2.setReserv_qty(2);
        txn2.setPickupMethod(pickupMethod_A);
        ticketReserveTxnFacade.create(txn2);
        ticketReserveTxns.add(txn2);
                
    }
    
    @After
    public void tearDown() {
        // get the entity manager
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("TicketCare-ProductionPU");
        EntityManager em = factory.createEntityManager();
        // remove the ticket, receiver and reservation txn data 
        // remove Ticking record
        String del_str = "delete from TicketingEntity t where t.ticket = :ticket";
        EntityTransaction emTxn = em.getTransaction();
        emTxn.begin();
        for (Ticket ticket: tickets){
            em.createQuery(del_str).setParameter("ticket", ticket).executeUpdate();
        }
        emTxn.commit();
        
        ticketReserveTxns.forEach((txn) -> {
           ticketReserveTxnFacade.remove(txn);
        });
        
        for (Ticket ticket: tickets){
            ticketDataFacade.remove(ticket);
        }
        // remove the ticking data
        receivers.forEach((r) -> {
            receiverFacade.remove(r);
        });
       
         for(TicketPickUpMethod method: pickupMethods){
            ticketPickUpMethodFacade.remove(method);
        }
        em.close();
        factory.close();
    }

    
    
    /**
     * Test of findValidReservQty method, of class TicketReserveTxnFacade.
     */
    @Test
    public void testFindValidReservQty() throws Exception {
        System.out.println("findValidReservQty");
        int expResult = 1;
        int result = ticketReserveTxnFacade.findValidReservQty().size();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of updateReserveTxnStatus method, of class TicketReserveTxnFacade.
     */
    @Test
    public void testUpdateReserveTxnStatus() throws Exception {
        System.out.println("updateReserveTxnStatus");
        List<TicketReserveTxnFacade.ReservTxnSummary> summary = ticketReserveTxnFacade.findValidReservQty();
        boolean expResult = true;
        boolean result = ticketReserveTxnFacade.updateReserveTxnStatus(summary);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of transfer2Ticketing method, of class TicketReserveTxnFacade.
     */
    @Test
    public void testTransfer2Ticketing() throws Exception {
        System.out.println("transfer2Ticketing");
        List<TicketReserveTxnFacade.ReservTxnSummary> summary = ticketReserveTxnFacade.findValidReservQty();;
        int expResult = 1;
        int result = ticketReserveTxnFacade.transfer2Ticketing(summary);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
}
