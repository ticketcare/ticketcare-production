/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Ticket Reservation Transaction
 *
 * @author user
 */
@Entity
public class DispatchEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @TableGenerator(name = "DispatchEntity_SEQ", table = "SEQ_TABLE", pkColumnValue = "DispatchEntity")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "DispatchEntity_SEQ")
    private Long id;

    /**
     * Date to generate the txn.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date txn_timestamp;

    /**
     * Date to execute the txn.
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date txn_execute_timestamp;

    private Long ticket_id;
    /**
     * 個案編號
     */
    private Long receiver_id;
    private int reserv_qty;
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    /**
     * 交易是否已被執行
     */
    boolean isExecute;

    /**
     * 記錄志工取票方式
     */
    @OneToOne
    @JoinColumn(name = "PICKUP_METHOD_ID", referencedColumnName = "ID")
    private TicketPickUpMethod pickupMethod;

    public TicketPickUpMethod getPickupMethod() {
        return pickupMethod;
    }

    public void setPickupMethod(TicketPickUpMethod pickupMethod) {
        this.pickupMethod = pickupMethod;
    }

    public boolean isIsExecute() {
        return isExecute;
    }

    public void setIsExecute(boolean isExecute) {
        this.isExecute = isExecute;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTxn_timestamp() {
        return txn_timestamp;
    }

    public void setTxn_timestamp(Date txn_timestamp) {
        this.txn_timestamp = txn_timestamp;
    }

    public Long getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(Long ticket_id) {
        this.ticket_id = ticket_id;
    }

    public Long getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(Long receiver_id) {
        this.receiver_id = receiver_id;
    }

    public int getReserv_qty() {
        return reserv_qty;
    }

    public void setReserv_qty(int reserv_qty) {
        this.reserv_qty = reserv_qty;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getTxn_execute_timestamp() {
        return txn_execute_timestamp;
    }

    public void setTxn_execute_timestamp(Date txn_execute_timestamp) {
        this.txn_execute_timestamp = txn_execute_timestamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DispatchEntity)) {
            return false;
        }
        DispatchEntity other = (DispatchEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TicketReserveTxn[ id=" + id + " ]";
    }

}
