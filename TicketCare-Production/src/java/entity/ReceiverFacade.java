/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.tcare.jpa.utils.AbstractFacade;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * {@link AbstractFacade<T> 提供的方法說明請參考 {@link https://gist.github.com/hychen39/d370238f0ffac8c2c4ec71d950c3d501}
 *
 * @author steven
 */
@Stateless
@LocalBean
public class ReceiverFacade extends AbstractFacade<Receiver> {

    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;
    private Logger logger = Logger.getLogger(ReceiverFacade.class.getName());
    private String name;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * 必要的預設建構子，初始化父類別
     */
    public ReceiverFacade() {
        super(Receiver.class);
    }

    /**
     * Find receivers by the given category:{@code TicketCategoryEntity}.
     *
     * @param category
     * @return
     */
    public List<Receiver> findByCategory(TicketCategoryEntity category) {
        List<Receiver> candidates = null;
        try {
            TypedQuery<Receiver> query = em.createNamedQuery("findByCategory", Receiver.class);
            query.setParameter("category", category);
            candidates = query.getResultList();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Cannot find Receivers by ticket category.");
            return null;
        }
        return candidates;
    }
}
