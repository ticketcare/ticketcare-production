/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.tcare.jpa.utils.AbstractFacade;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * {@link AbstractFacade<T> 提供的方法說明請參考 {@link https://gist.github.com/hychen39/d370238f0ffac8c2c4ec71d950c3d501}
 *
 * @author steven
 */
@Stateless
@LocalBean
public class VolunteerFacade extends AbstractFacade<Volunteer> {

    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * 必要的預設建構子，初始化父類別
     */
    public VolunteerFacade() {
        super(Volunteer.class);
    }
}
