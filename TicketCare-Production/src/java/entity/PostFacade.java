/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.tcare.jpa.utils.AbstractFacade;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * {@link AbstractFacade<T> 提供的方法說明請參考 {@link https://gist.github.com/hychen39/d370238f0ffac8c2c4ec71d950c3d501}
 *
 * @author steven
 */
@Stateless
@LocalBean
public class PostFacade extends AbstractFacade<Post> {

    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;

    private String name;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * 必要的預設建構子，初始化父類別
     */
    public PostFacade() {
        super(Post.class);
    }

    /**
     *
     * @param ticket
     * @return
     */
    public Post findPostByTicket(Ticket ticket) {
        Post resultPost = null;
        try {

            TypedQuery<Post> query = em.createNamedQuery("findPostByTicket", Post.class).setParameter("ticket_in", ticket);
            resultPost = query.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
        return resultPost;
    }

    /**
     * 依日期排序 Post，傳回 {@code startPos} 開始的 {@code rows} 筆資料.
     *
     * @param startPos 第一筆資料的位置, 位置從 0 開始。
     * @param rows 回傳的筆數
     * @return List of Post entity
     * 
     * @see Post  
     *
     * Example: {@code findPostsOrderByStartDate(0, 5)} 傳回第 1~5 筆(位置編號 0~4)資料。
     */
    public List<Post> findPostsOrderByStartDate(int startPos, int rows) {
        List<Post> resultPosts = null;
        TypedQuery<Post> query = em.createNamedQuery("findPostOrderByDate", Post.class)
                .setFirstResult(startPos)
                .setMaxResults(rows);
        resultPosts = query.getResultList();
        return resultPosts;
    }
}
