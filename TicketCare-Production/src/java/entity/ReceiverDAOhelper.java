package entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hychen39@gmail.com
 */
public class ReceiverDAOhelper {

    static private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    /**
     * Convert the fields in {@link Receiver} object to a string with CSV
     * format.
     *
     * Columns: (0)idCardNumber, (1)name, (2)gender, (3)birth, (4)phoneNumber,
     * (5) mobileNumber, (6) address, (7)interesting, (8)note
     *
     * Note: 若使用 JPA，就無需使用 DAOHelper 來做 object 及 record 之間的轉換。
     *
     * @param receiver
     * @return
     */
    static public String getFromatStr(Receiver receiver) {
        String formatStr = null;
        StringBuilder sb = new StringBuilder();
        sb.append(receiver.getIdNumber()).append(", ");
        sb.append(receiver.getName()).append(", ");
        sb.append(receiver.getGender()).append(", ");

        String birthStr = sdf.format(receiver.getBirth());
        sb.append(birthStr).append(", ");
        sb.append(receiver.getArea_code()).append(", ");
        sb.append(receiver.getTel()).append(", ");
        sb.append(receiver.getAddress()).append(", ");

        StringBuilder interestings = new StringBuilder();
        if (receiver.getInterestings() != null) {
            for (String i : receiver.getInterestings()) {
                interestings.append(i).append("-");
            }

        } else {
            interestings.append("null");
        }
        sb.append(interestings.toString()).append(", ");
        if (receiver.getNote() != null && !"".equals(receiver.getNote())) {
            sb.append(receiver.getNote());
        } else {
            sb.append("null");
        }

        formatStr = sb.toString();
        return formatStr;
    }

    /**
     * Parse the string generate from
     * {@link #getFromatStr(com.ticketCare.entities.Receiver)} to generate a
     * {@code Receiver} object
     *
     * @param fmtStr formated string
     * @return {@code Receiver} object
     */
    static public Receiver parseFormatStr(String fmtStr) {
        if (fmtStr == null) {
            return null;
        }
        Receiver receiver = new Receiver();

        String[] fields;
        fields = fmtStr.split(",\\s*");
        receiver.setIdNumber(fields[0]);
        receiver.setName(fields[1]);
        receiver.setGender(fields[2]);
        try {
            //Date
            Date birthDate = sdf.parse(fields[3]);
            receiver.setBirth(birthDate);
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverDAOhelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        receiver.setArea_code(fields[4]);
        receiver.setTel(fields[5]);
        receiver.setAddress(fields[6]);

        if (fields[7] != null) {
            String[] interistings = fields[6].split("-");
            receiver.setInterestings(Arrays.asList(interistings));
        } else {
            receiver.setInterestings(new ArrayList<String>());
        }
        receiver.setNote(fields[8]);

        return receiver;
    }
}
