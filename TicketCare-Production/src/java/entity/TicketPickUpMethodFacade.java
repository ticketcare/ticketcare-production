/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.tcare.jpa.utils.AbstractFacade;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * {@link AbstractFacade<T> 提供的方法說明請參考 {@link https://gist.github.com/hychen39/d370238f0ffac8c2c4ec71d950c3d501}
 *
 * @author steven
 */
@Stateless
@LocalBean
public class TicketPickUpMethodFacade extends AbstractFacade<TicketPickUpMethod> {

    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * 必要的預設建構子，初始化父類別
     */
    public TicketPickUpMethodFacade() {
        super(TicketPickUpMethod.class);
    }

    public void populateContent() {
        if (this.findAll().size() < 1) {
            TicketPickUpMethod byYourself = new TicketPickUpMethod();
            byYourself.setId(new Long(1));
            byYourself.setWay("自取");
            TicketPickUpMethod byVolunteer = new TicketPickUpMethod();
            byVolunteer.setId(new Long(2));
            byVolunteer.setWay("志工");
            super.edit(byYourself);
            super.edit(byVolunteer);
        }
    }
}
