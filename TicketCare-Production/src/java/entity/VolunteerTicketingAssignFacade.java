/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.tcare.jpa.utils.AbstractFacade;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author user
 */
@Stateless
@LocalBean
public class VolunteerTicketingAssignFacade extends AbstractFacade<VolunteerTicketingAssign> {
    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;
    
    public VolunteerTicketingAssignFacade() {
        super(VolunteerTicketingAssign.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
