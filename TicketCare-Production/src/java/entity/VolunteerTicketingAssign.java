/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 志工認領取票票工作的交易
 * @author user
 */
@Entity
@Table(name = "VOLUNTEER_TICKETING_ASSIGN")
public class VolunteerTicketingAssign implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    /** 志工編號 */
    private Long volunteer_id;
    
    /** 出票編號 */
    private Long ticketing_id;
    
    /** 工作指派日期 */
    @Temporal(TemporalType.DATE)
    private Date assign_date;

    public Long getVolunteer_id() {
        return volunteer_id;
    }

    public void setVolunteer_id(Long volunteer_id) {
        this.volunteer_id = volunteer_id;
    }

    public Long getTicketing_id() {
        return ticketing_id;
    }

    public void setTicketing_id(Long ticketing_id) {
        this.ticketing_id = ticketing_id;
    }

    public Date getAssign_date() {
        return assign_date;
    }

    public void setAssign_date(Date assign_date) {
        this.assign_date = assign_date;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VolunteerTicketingAssign)) {
            return false;
        }
        VolunteerTicketingAssign other = (VolunteerTicketingAssign) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Volunteer_Method[ id=" + id + " ]";
    }
    
}
