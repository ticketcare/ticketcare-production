/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.time;
import com.tcare.jpa.utils.AbstractFacade;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * {@link AbstractFacade<T> 提供的方法說明請參考 {@link https://gist.github.com/hychen39/d370238f0ffac8c2c4ec71d950c3d501}
 *
 * @author steven
 */
@Stateless
@LocalBean
public class TicketDataFacade extends AbstractFacade<Ticket> {

    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * 取得到現在為止仍有效期的票劵
     *
     * @return
     */
    public List<Ticket> getEffectiveTicket() {
        LocalDate localDate = null;
        List<Ticket> resultPosts = null;

        localDate = LocalDate.now();
        Date currentDate = java.sql.Date.valueOf(localDate);
        TypedQuery<Ticket> query = em.createNamedQuery("findEffectiveTicket", Ticket.class);
        query.setParameter("current_date", currentDate);
        resultPosts = query.getResultList();
        return resultPosts;
    }

    /**
     * 必要的預設建構子，初始化父類別
     */
    public TicketDataFacade() {
        super(Ticket.class);
    }
}
