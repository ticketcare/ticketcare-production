/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 出票記錄實體
 * @author user
 */
@Entity
public class TicketingEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @TableGenerator(name = "Ticketing_SEQ", table = "SEQ_TABLE", pkColumnValue = "Ticketing")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "Ticketing_SEQ")
    private Long id;
    
    // private member
    
    @ManyToOne
    private Ticket ticket;
    
    @ManyToOne
    private Receiver receiver;
    
    private int qty;
    
    /** 預計取票日期
     */
    @Temporal(TemporalType.DATE)
    private Date plannedPickupDate;
    
    /**
     * 實際取票日期
     */
    @Temporal(TemporalType.DATE)
    private Date pickupDate;
    
    @ManyToOne
    private TicketPickUpMethod pickupMethod;
    
    @ManyToOne
    private VolunteerTicketingAssign volunteerGive;
    
    /**
     * Status code. 1: Open. 9:close 
     */
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Date getPlannedPickupDate() {
        return plannedPickupDate;
    }

    public void setPlannedPickupDate(Date plannedPickupDate) {
        this.plannedPickupDate = plannedPickupDate;
    }

    public TicketPickUpMethod getPickupMethod() {
        return pickupMethod;
    }

    public void setPickupMethod(TicketPickUpMethod pickupMethod) {
        this.pickupMethod = pickupMethod;
    }

    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    public VolunteerTicketingAssign getVolunteerGive() {
        return volunteerGive;
    }

    public void setVolunteerGive(VolunteerTicketingAssign volunteerGive) {
        this.volunteerGive = volunteerGive;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TicketingEntity)) {
            return false;
        }
        TicketingEntity other = (TicketingEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TickingEntity[ id=" + id + " ]";
    }
    
}
