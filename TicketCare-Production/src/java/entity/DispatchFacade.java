/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.tcare.jpa.utils.AbstractFacade;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author user
 */
@Stateless
@LocalBean
public class DispatchFacade extends AbstractFacade<DispatchEntity> {

    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;

    private String name;

    public DispatchFacade() {
        super(DispatchEntity.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
