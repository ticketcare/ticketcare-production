package entity;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Data access object for Receiver entity class. Note: 若使用 JPA，就無需使用 DAO object
 * 來存取文字檔中的內容。 會使用 {@link TicketCategoryFacade} 存取資料庫中的內容。
 *
 * @author hychen39@gmail.com
 */
public class receiverDAO {

    private String filename;
    private String path;
    List<Receiver> receivers;
    List<Receiver> newReceivers;

    public List<Receiver> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<Receiver> receivers) {
        this.receivers = receivers;
    }

    public List<Receiver> getNewReceivers() {
        return newReceivers;
    }

    public void setNewReceivers(List<Receiver> newReceivers) {
        this.newReceivers = newReceivers;
    }

    /**
     * Get all receivers.
     *
     * @return
     */
    public List<Receiver> getAll() {
        // load to field at the first time. 
        if (receivers == null) {
            loadFromFile();
        }
        return receivers;
    }

    /**
     * Write a new receiver to file;
     *
     * @param List
     */
    public void write(List<Receiver> receivers) {
        FileWriter fw = null;
        try {
            // open file
            Path fullname = Paths.get(getPath(), getFilename());
            fw = new FileWriter(fullname.toString());
            PrintWriter pw = new PrintWriter(fw, true);
            for (Receiver r : receivers) {
                pw.println(ReceiverDAOhelper.getFromatStr(r));
            }
            pw.close();
        } catch (IOException ex) {
            Logger.getLogger(receiverDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(receiverDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Add the new {@code Receiver} instance to buffer and save to file.
     *
     * @param newReceiver
     */
    public void save(Receiver newReceiver) {
        //get the current list
        List<Receiver> allList = getAll();
        allList.add(newReceiver);

        write(allList);
    }

    /**
     * Convert the fields in {@link Receiver} object to a string with CSV
     * format.
     *
     * Columns: idCardNumber, name, gender, birth, phoneNumber, mobileNumber,
     * address, interesting, note
     *
     * @param receiver
     * @return
     * @deprecated use the method in {@link ReceiverDAOHelper}.
     */
    public String getFromatStr(Receiver receiver) {
        String formatStr = null;
        StringBuilder sb = new StringBuilder();
        //column 0
        sb.append(receiver.getIdNumber()).append(", ");
        //column 1
        sb.append(receiver.getName()).append(", ");
        //column 2
        sb.append(receiver.getGender()).append(", ");
        //column 3
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String birthStr = sdf.format(receiver.getBirth());
        sb.append(birthStr).append(", ");
        //column 4
        sb.append(receiver.getArea_code()).append(", ");
        //column 5
        sb.append(receiver.getTel()).append(", ");
        //column 6
        sb.append(receiver.getAddress()).append(", ");
        //column 7
        StringBuilder interestings = new StringBuilder();
        if (receiver.getInterestings() != null) {
            for (String i : receiver.getInterestings()) {
                interestings.append(i).append("-");
            }
        }
        sb.append(interestings.toString());
        //column 8
        sb.append(receiver.getNote());

        formatStr = sb.toString();
        return formatStr;
    }

    /**
     * Load data to {@link #receivers} from the external file specified by
     * {@link #path} and {@link #filename}
     */
    private void loadFromFile() {
        receivers = new ArrayList<>();

        FileReader fr;
        BufferedReader br = null;
        try {
            fr = new FileReader(getFullname());
            br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                Receiver receiver = ReceiverDAOhelper.parseFormatStr(line);
                receivers.add(receiver);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(receiverDAO.class.getName()).log(Level.SEVERE, null, ex);
            //ToDo: 
        } catch (IOException ex) {
            Logger.getLogger(receiverDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    Logger.getLogger(receiverDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Get the full filename given the {@link #path} and {@link #filename}
     * fields.
     *
     * @return
     */
    public String getFullname() {
        Path fullname = Paths.get(getPath(), getFilename());
        return fullname.toString();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
