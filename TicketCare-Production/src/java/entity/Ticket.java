package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Ticket Entity
 *
 * @author user
 * @since 2017/8/2
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name = "findEffectiveTicket",
            query = "select t from Ticket t where t.endDate > :current_date")})
@Table(name = "Ticket") // Override the default table name to Ticket_Category
public class Ticket implements Serializable {

    public enum Status {
        OFF_SHELF, ON_SHELF
    };

    @TableGenerator(name = "TICKET_SEQ", table = "SEQ_TABLE", pkColumnValue = "TICKET")

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TICKET_SEQ")
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_date;
    @OneToOne
    private TicketCategoryEntity category;
    private String name;
    /**
     * 捐贈票劵是的數量，會和 onHandQty 數量同步。
     */
    private int qty;
    /**
     * 票劵在手數量 (On-hand qty)
     */
    @Column(name = "ON_HAND_QTY")
    private int onHandQty;
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Temporal(TemporalType.DATE)
    private Date startDate;
    private String content;
    private String address;
    @ManyToOne  // Unidirectional Many-to-one with a dornor. 
    private DonorEntity donor;
    @Enumerated
    private Status status = Status.OFF_SHELF;
    @Lob
    private byte[] photo;

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public TicketCategoryEntity getCategory() {
        return category;
    }

    public void setCategory(TicketCategoryEntity category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        this.onHandQty = qty; // 在手數量 = 捐贈時的數量。
    }

    public int getOnHandQty() {
        return onHandQty;
    }

    public void setOnHandQty(int onHandQty) {
        this.onHandQty = onHandQty;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public DonorEntity getDonor() {
        return donor;
    }

    public void setDonor(DonorEntity donor) {
        this.donor = donor;
    }
}
