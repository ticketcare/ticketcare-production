/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
/**
 *
 * @author BoYenLai
 */
@Entity
public class Questionnaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToOne
    private TicketingEntity ticketing;
    private String ticket_use;
    private String activity_experience;
    private String who_use;
    private String participation;
    private String satisfaction;
    private String next_chance;
    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TicketingEntity getTicketing() {
        return ticketing;
    }

    public void setTicketing(TicketingEntity ticketing) {
        this.ticketing = ticketing;
    }

    public String getTicket_use() {
        return ticket_use;
    }

    public void setTicket_use(String ticket_use) {
        this.ticket_use = ticket_use;
    }

    public String getActivity_experience() {
        return activity_experience;
    }

    public void setActivity_experience(String activity_experience) {
        this.activity_experience = activity_experience;
    }

    public String getWho_use() {
        return who_use;
    }

    public void setWho_use(String who_use) {
        this.who_use = who_use;
    }

    public String getParticipation() {
        return participation;
    }

    public void setParticipation(String participation) {
        this.participation = participation;
    }

    public String getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(String satisfaction) {
        this.satisfaction = satisfaction;
    }

    public String getNext_chance() {
        return next_chance;
    }

    public void setNext_chance(String next_chance) {
        this.next_chance = next_chance;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questionnaire)) {
            return false;
        }
        Questionnaire other = (Questionnaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Questionnaire[ id=" + id + " ]";
    }
    
}
