package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 個案 Entity
 *
 * @author user
 */
@Entity
@Table(name = "Receiver")
@NamedQueries({
    @NamedQuery(name = "findByCategory",
            query = "select r from Receiver r where :category member of r.interests")})
public class Receiver implements Serializable {

    @TableGenerator(name = "RECEIVER_SEQ", table = "SEQ_TABLE", pkColumnValue = "RECEIVER")

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "RECEIVER_SEQ")
    private Long id;
    private String name;
    private String idNumber;
    @Temporal(TemporalType.DATE)
    private Date birth;
    private String gender;
    private String area_code;
    private String tel;
    private String address;
    @Deprecated
    @ElementCollection
    private List<String> interestings;
    @OneToMany
    private List<TicketCategoryEntity> interests;
    private String note;

    public Receiver() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getArea_code() {
        return area_code;
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Deprecated
    public List<String> getInterestings() {
        return interestings;
    }

    @Deprecated
    public void setInterestings(List<String> interestings) {
        this.interestings = interestings;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<TicketCategoryEntity> getInterests() {
        return interests;
    }

    public void setInterests(List<TicketCategoryEntity> interests) {
        this.interests = interests;
    }

}
