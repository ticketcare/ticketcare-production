/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.tcare.jpa.utils.AbstractFacade;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * 票卷保留交易 Facade
 * @author user
 * @see TicketReserveTxn
 */
@Stateless
@LocalBean
public class TicketReserveTxnFacade extends AbstractFacade<TicketReserveTxn> {
    
    @EJB TicketDataFacade ticketFacade;
    
    @EJB ReceiverFacade receiverFacade;
    
    @EJB TicketPickUpMethodFacade pickUpMethodFacade;
    
    @EJB TicketingFacade ticketingFacade;
    
    
    public class ReservTxnSummary {
        private Long ticketID;
        private Long receiverID;
        private Long qty;
        private Long pickupMethod;
        private Date dueDate;

        public Date getDueDate() {
            return dueDate;
        }

        public void setDueDate(Date dueDate) {
            this.dueDate = dueDate;
        }

        public Long getTicketID() {
            return ticketID;
        }

        public void setTicketID(Long ticketID) {
            this.ticketID = ticketID;
        }

        public Long getReceiverID() {
            return receiverID;
        }

        public void setReceiverID(Long receiverID) {
            this.receiverID = receiverID;
        }

        public Long getQty() {
            return qty;
        }

        public void setQty(Long qty) {
            this.qty = qty;
        }

        public Long getPickupMethod() {
            return pickupMethod;
        }

        public void setPickupMethod(Long pickupMethod) {
            this.pickupMethod = pickupMethod;
        }
        
        
    }

    @PersistenceContext(name = "TicketCare-ProductionPU")
    private EntityManager em;

    public TicketReserveTxnFacade() {
        super(TicketReserveTxn.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    /**
     * ToDo: Debug.
     * Cause exception: You have attempted to set a parameter value using a name of receiver_id that does not exist in the query string select txn.receiver_id, txn.ticket_id, sum(txn.reserv_qty) from TicketReserveTxn txn where txn.executeCode = 1 group by txn.receiver_id, txn.ticket_id.
     * @param receiverID
     * @param ticketID
     * @return 
     */
    public Long getResvQty(Long receiverID, Long ticketID) {
        TypedQuery<Long> query = em.createNamedQuery("getReservQty", Long.class);
        query.setParameter("receiver_id", receiverID).setParameter("ticket_id", ticketID);
        Long value = query.getSingleResult();
        return value;
    }
    
    /**
     * 取得有效的票卷保留記錄，以供轉成出票記錄。
     * @return 
     */
    public List<ReservTxnSummary> findValidReservQty(){
        final String QUERY_STR = 
           "select txn.receiver_id, txn.ticket_id, txn.pickupMethod, sum(txn.reserv_qty), min(txn.dueDate) from TicketReserveTxn txn "
            + "where txn.executeCode = 1 group by txn.receiver_id, txn.ticket_id, txn.pickupMethod";  
        List<ReservTxnSummary> validReservQty = new ArrayList<>();
        
        Query query = em.createQuery(QUERY_STR);
        // make query
        List<Object []> txnSummary = query.getResultList();
        // convert to object
        for (Object [] txn: txnSummary){
            ReservTxnSummary summaryObj = new ReservTxnSummary();
            summaryObj.setReceiverID((Long) txn[0]);
            summaryObj.setTicketID((Long) txn[1]);
            summaryObj.setPickupMethod(((TicketPickUpMethod) txn[2]).getId());
            summaryObj.setQty((Long) txn[3]);
            summaryObj.setDueDate((Date) txn[4]);
            validReservQty.add(summaryObj);
        }
        
        return validReservQty;
    }
    
    /**
     * 給予一個出票清單, 更新其對應的票卷保留交易狀態, 交易的狀態碼改為 2 (executeCode = 2)
     * @param summary 出票清單 
     * @return true if update more than one records.
     */
    public boolean updateReserveTxnStatus(List<ReservTxnSummary> summary){
        final String UPDATE_STR = "update TicketReserveTxn txn set txn.executeCode = 2" 
                                + " where txn.ticket_id = :ticket_id and txn.receiver_id= :receiver_id";
        int update_counts = 0;
        for (ReservTxnSummary s: summary){
            Query query = em.createQuery(UPDATE_STR).setParameter("ticket_id", s.ticketID).setParameter("receiver_id", s.receiverID);
            update_counts += query.executeUpdate();
        }
        return (update_counts >0);
    }
    
    /**
     * 將出票清單記錄拋轉到出票記錄表格。
     * @param summary  
     * @return 拋轉的筆數
     * @see ReservTxnSummary
     */
    public int transfer2Ticketing(List<ReservTxnSummary> summary){
        int counts = 0;
        for(ReservTxnSummary txn: summary){
            TicketingEntity ticketingEntity = new TicketingEntity();
            // prepare attibutes required for TicketingEntity
            Ticket ticket = ticketFacade.find(txn.ticketID);
            Receiver receiver = receiverFacade.find(txn.receiverID);
            TicketPickUpMethod method = pickUpMethodFacade.find(txn.pickupMethod);
            // create TicketingEntity object
            ticketingEntity.setTicket(ticket);
            ticketingEntity.setReceiver(receiver);
            ticketingEntity.setPickupMethod(method);
            ticketingEntity.setQty(txn.getQty().intValue());
            ticketingEntity.setPlannedPickupDate(txn.getDueDate());
            ticketingEntity.setVolunteerGive(null);
            ticketingEntity.setStatus(1); // Status Code: Open
            ticketingEntity.setPickupDate(null);
            // persist 
            ticketingFacade.create(ticketingEntity);
            counts++;
        }
        return counts;
    }
    
}
