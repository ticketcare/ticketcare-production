package manageBean;

import java.util.*;
import entity.PostFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import entity.Post;
import entity.Ticket;
import entity.TicketDataFacade;

/**
 *
 * @author user
 */
@Named(value = "postBean")
@SessionScoped
public class PostBean implements Serializable {
    
    @EJB
    private PostFacade postFacade;
    @EJB
    private TicketDataFacade ticketFacade;
    private Post currentPost;

    /**
     * The ticket in the selected post.
     */
    private Ticket selectedTicket;

    public Ticket getSelectedTicket() {
        return selectedTicket;
    }

    public void setSelectedTicket(Ticket selectedTicket) {
        this.selectedTicket = selectedTicket;
    }

    public Post getCurrentPost() {
        return currentPost;
    }

    public List<Post> getAllPosts() {
        return postFacade.findAll();
    }

    /**
     * Create a new Post Entity and persistent to database.
     *
     * @param ticket
     * @return
     */
    public String addPostDB(Ticket ticket) {
        ticket.setStatus(Ticket.Status.ON_SHELF);
        ticketFacade.edit(ticket);
        currentPost = new Post();
        currentPost.setStartDate(getCurrentDate());
        currentPost.setTicket(ticket);
        postFacade.create(currentPost);
        return null;
    }

    /**
     * 下架功能. 下架要從 ticket 找到 post. 之後 Post 的狀態改成 OFF_SHELF
     *
     * @param ticket
     * @return
     */
    public String delPostDB(Ticket ticket) throws Exception {
        //ToDo: Need to find the post by giving the ticket. Then, remove the post from the database.
        Post postInstance = postFacade.findPostByTicket(ticket);
        if (postInstance != null) {
            postFacade.remove(postInstance);
        } else {
            throw new Exception("Cannot find Post by Given Ticket Name: " + ticket.getName());
        }
        // update the ticket status 
        ticket.setStatus(Ticket.Status.OFF_SHELF);
        ticketFacade.edit(ticket);
        return null;
    }

    public void PostCancel() {
        currentPost = null;
    }

    public PostFacade getPostFacade() {
        return postFacade;
    }

    public void setPostFacade(PostFacade postFacade) {
        this.postFacade = postFacade;
    }

    public TicketDataFacade getTicketFacade() {
        return ticketFacade;
    }

    public void setTicketFacade(TicketDataFacade ticketFacade) {
        this.ticketFacade = ticketFacade;
    }

    public List<Post> showcurrentpost(int size) {
        List<Post> list = new ArrayList<Post>();
        for (int i = 0; i < size; i++) {
            list.add(currentPost);
        }
        return list;
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public Date prepareCreateDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public Post findByTicket(Ticket ticket) {
        return postFacade.findPostByTicket(ticket);
    }

    /**
     * Call {@link PostFacade#findPostsOrderByStartDate(int, int) to find posts.
     *
     * @param startPos
     * @param rows
     * @return @see PostFacade
     */
    public List<Post> findPostsOrderByDate(int startPos, int rows) {
        return postFacade.findPostsOrderByStartDate(startPos, rows);
    }
    
    /**
     * Find all the posts ordered by date.
     * 
     * Call {@link PostFacade#findPostsOrderByStartDate(int, int) 
     * @return 
     */
    public List<Post> findPostsOrderByDate(){
        int count = postFacade.count();
        return postFacade.findPostsOrderByStartDate(0, count);
    }

    /**
     * View ticket detail by the post ID.
     *
     * @param ticketID Post ID
     * @return target page id: ticketDetails
     */
    public String viewTicket(Long ticketID) {

        // find the ticket from the database
        Ticket ticket = ticketFacade.find(ticketID);

        if (ticket != null) {
            selectedTicket = ticket;
            // redirect to target page
            return "ticketDetails";
        }
        // not redirect.
        return null;
    }
    
    /**
     * Return the number of posts in the database.
     * 
     * @return 
     */
    public int getPostCounts(){
        int counts = 0;
        counts = postFacade.count();
        return counts;
    }
    
}
