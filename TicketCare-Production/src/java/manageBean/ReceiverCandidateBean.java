/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import com.tcare.biz_rules.TicketOperationRules;
import com.tcare.web.util.JsfUtil;
import entity.Receiver;
import entity.ReceiverFacade;
import entity.Ticket;
import entity.TicketCategoryEntity;
import entity.TicketDataFacade;
import entity.TicketReserveTxn;
import entity.TicketReserveTxnFacade;
import entity.TicketPickUpMethodFacade;
import entity.TicketPickUpMethod;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.context.RequestContext;

/**
 * Managed Bean for <code>receiverByCategoryReport.xhtml </code>
 *
 * @author user
 */
@Named(value = "receiverCandidateBean")
@SessionScoped
public class ReceiverCandidateBean implements Serializable {

    static public final String TARGET_PAGE = "RECEIVER_BY_CATEGORY";
    private Ticket selectedTicket;

    private TicketCategoryEntity category;

    @EJB
    private ReceiverFacade receiverFacade;
    @EJB
    private TicketReserveTxnFacade ticketResvFacade;
    @EJB
    private TicketDataFacade ticketFacace;

    private TicketReserveTxn ticketTxn;

    public TicketCategoryEntity getCategory() {
        return category;
    }

    public void setCategory(TicketCategoryEntity category) {
        this.category = category;
    }

    /**
     * Creates a new instance of ReceiverCandidateBean
     */
    public ReceiverCandidateBean() {
    }

    //Getter and Setters
    public Ticket getSelectedTicket() {
        return selectedTicket;
    }

    public void setSelectedTicket(Ticket selectedTicket) {
        this.selectedTicket = selectedTicket;
    }

    public TicketReserveTxn getTicketTxn() {
        return ticketTxn;
    }

    public void setTicketTxn(TicketReserveTxn ticketTxn) {
        this.ticketTxn = ticketTxn;
    }

    //Methods
    /**
     * Actions before the page.
     *
     * @param ticket selected ticket by user.
     * @return target page string.
     */
    public String preparePage(Ticket ticket) {
        selectedTicket = ticket;
        return TARGET_PAGE;
    }

    public List<Receiver> findReceiverByInterest(TicketCategoryEntity interest) {
        List<Receiver> candidates = null;
        candidates = receiverFacade.findByCategory(interest);
        return candidates;
    }

    public List<TicketReserveTxn> getAllTicketReserveTxn() {
        return ticketResvFacade.findAll();
    }

    /**
     * Generate a ticket reservation transaction and show a dialog to ask user
     * to input reserved quantity and the due date to get the ticket.
     *
     * @param receiver
     */
    public void prepareReservTxn(Receiver receiver) {
        this.ticketTxn = new TicketReserveTxn();
        //date to generate the txn.
        ticketTxn.setTxn_timestamp(JsfUtil.getCurrentDate());
        ticketTxn.setReceiver_id(receiver.getId());
        ticketTxn.setTicket_id(selectedTicket.getId());
//        ticketTxn.setReceiver_name(receiver);
//        ticketTxn.setTicket_name(selectedTicket);
//        ticketTxn.setCategory(selectedTicket.getCategory());
        ticketTxn.setExecuteCode(0);
        ticketTxn.setDueDate(TicketOperationRules.ticketPickupDueDate());
//        ticketTxn.setVolunteerTakeWayEntity(receiver.);

        // show the dialog
        // Use <p:commandButton> onComplete event to show the dialog, instead of showing from the server side.
//        RequestContext context = RequestContext.getCurrentInstance();
//        context.execute("PF('dlgResvTicket').show();");
    }

    /**
     * Execute the ticket reservation transaction.
     */
    public void execReservTxn(TicketReserveTxn ticketReservTxn) {
        final String SUCCESS_MSG = "On-hand qty of Ticket %d changes from %d to %d ";
//        final String FAIL_MSG = "";
        // persist the txn object. 
        ticketResvFacade.create(ticketTxn);

        // change the on-hadn qty of the ticket
        selectedTicket = ticketFacace.find(ticketTxn.getTicket_id());
        int curQty = selectedTicket.getOnHandQty();
        selectedTicket.setOnHandQty(curQty - ticketTxn.getReserv_qty());
        // persist the ticket
        ticketFacace.edit(selectedTicket);

        // change the status of txn and update the date of executing the txn.
        ticketTxn.setExecuteCode(1);
        ticketTxn.setTxn_execute_timestamp(JsfUtil.getCurrentDate());
        // persist the txn objec.
        ticketResvFacade.edit(ticketTxn);

        // Show message to inform the user.
        String msg = SUCCESS_MSG.format(SUCCESS_MSG, selectedTicket.getId(), curQty, selectedTicket.getOnHandQty());
        JsfUtil.addSuccessMessage(msg);
    }

    /**
     * TODO
     * @param receiverID
     * @param ticketID
     * @return 
     */
    public Long findReservQty(Long receiverID, Long ticketID) {
        Long qty = null;
        qty = ticketResvFacade.getResvQty(receiverID, ticketID);
        if (qty == null) {
            qty = new Long(0);
        }
        return qty;
    }

}
