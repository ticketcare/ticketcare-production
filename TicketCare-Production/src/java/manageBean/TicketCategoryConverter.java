/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package manageBean;

import com.tcare.web.util.JsfUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import entity.TicketCategoryFacade;
import entity.TicketCategoryEntity;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author hychen39@gmail.com
 */
@FacesConverter("ticketCategoryConverter")
public class TicketCategoryConverter implements Converter {

    @EJB
    TicketCategoryFacade ticketCategoryFacade;

    private Logger log = Logger.getLogger(DonorConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long pkValue = null;

        if (value == null) {
            String msg = "Please select a ticketCategory from list.";
            String errMsg = generateErrorMsg("Null", msg);
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
            return null;
        }

        try {
            pkValue = Long.parseLong(value);

        } catch (NumberFormatException ex) {
            String msg = ". Cannot get a valid category id to find ticketCategory instance.";
            String errMsg = generateErrorMsg(value, msg);
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
        }
        if (pkValue == null) {
            return null;
        }
        if (pkValue > 0) {
            return ticketCategoryFacade.find(pkValue);
        } else {
            return null;
        }
    }

    private String generateErrorMsg(String value, String msg) {
        StringBuilder msgbuilder = new StringBuilder();
        msgbuilder.append(DonorConverter.class.getName()).append(": Value: ");
        msgbuilder.append(value);
        msgbuilder.append(msg);
        return msgbuilder.toString();
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        TicketCategoryEntity currentTicketCategory;
        if (value == null) {
            return null;
        }

        try {
            currentTicketCategory = (TicketCategoryEntity) value;
        } catch (ClassCastException exception) {
            String errMsg = generateErrorMsg(value.toString(), ". Cannot cast to ticketCategory instance.");
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
            return null;
        }
        // return the object ID.
        return currentTicketCategory.getId().toString();
    }
}
