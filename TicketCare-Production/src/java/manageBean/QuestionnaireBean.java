/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import manageBean.TicketingBean;
import manageBean.ReceiverCandidateBean;
import entity.Questionnaire;
import entity.QuestionnaireFacade;
import entity.TicketingEntity;
import entity.TicketingFacade;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import org.primefaces.context.RequestContext;
/**
 *
 * @author BoYenLai
 */
@Named(value = "questionnaireBean")
@SessionScoped
public class QuestionnaireBean implements Serializable {
    
    final static public String QUESTIONNAIRE_ADD_PAGE = "ticketClosing-questionnaire";
    final static public String QUESTIONNAIRE_DATA_PAGE = "ticketTarck";
    final static public String QUESTIONNAIRE_VIEWDETAIL_PAGE = "Questionnaire-detail";
    
    
    enum FormMode {
        CREATE, EDIT
    }
    
    private FormMode formMode;
    

    @EJB
    private QuestionnaireFacade questionnaireFacade;
    
    private Questionnaire currentQuestionnaire;

    public FormMode getFormMode() {
        return formMode;
    }

    public void setFormMode(FormMode formMode) {
        this.formMode = formMode;
    }

    public QuestionnaireFacade getQuestionnaireFacade() {
        return questionnaireFacade;
    }

    public void setQuestionnaireFacade(QuestionnaireFacade questionnaireFacade) {
        this.questionnaireFacade = questionnaireFacade;
    }
    
    public List<Questionnaire> getAllQuestionnaire() {
        return questionnaireFacade.findAll();
    }

    public Questionnaire getCurrentQuestionnaire() {
        return currentQuestionnaire;
    }

    public void setCurrentQuestionnaire(Questionnaire currentQuestionnaire) {
        this.currentQuestionnaire = currentQuestionnaire;
    }

    /**
     * Creates a new instance of QuestionnaireBean
     */
    public QuestionnaireBean() {
    }
    
    public String saveQuestionnaire() {
        questionnaireFacade.create(currentQuestionnaire);
        return QUESTIONNAIRE_DATA_PAGE;
    }
    
    /**
     * Prepare the questionnaire page. 
     * @param ticketingRec Ticketing record to be closed.
     * @return 
     */
     public String prepareCreate(TicketingEntity ticketingRec) {
        currentQuestionnaire = new Questionnaire();
        currentQuestionnaire.setTicketing(ticketingRec);
        formMode = FormMode.CREATE;
        return QUESTIONNAIRE_ADD_PAGE;
    }
     
     public String viewQuestionnaireAction(Questionnaire questionnaire) {
        currentQuestionnaire = questionnaire;
        return QUESTIONNAIRE_VIEWDETAIL_PAGE;
    }
    
}
