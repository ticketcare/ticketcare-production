/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package manageBean;

import entity.Ticket;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Convert Ticket.Status to String and back-forward.
 *
 * @author hychen39@gmail.com
 */
@FacesConverter("ticketStatusConverter")
public class TicketStatusConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        // String to object
        if (value.toUpperCase().equals("ON-SHELF")) {
            return Ticket.Status.ON_SHELF;
        } else {
            return Ticket.Status.OFF_SHELF;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        // Object to String
        String status = "OFF_SHELF";
        if (value instanceof Ticket.Status) {
            if (((Ticket.Status) value).equals(Ticket.Status.ON_SHELF)) {
                status = "ON_SHELF";
            }
        }
        return status;
    }
}
