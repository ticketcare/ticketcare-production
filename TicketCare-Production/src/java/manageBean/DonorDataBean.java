/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import static com.sun.xml.ws.security.addressing.impl.policy.Constants.logger;
import java.util.*;
import entity.DonorFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import entity.DonorEntity;
import entity.Post;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author user
 */
@Named(value = "donorDataBean")
@SessionScoped
public class DonorDataBean implements Serializable {

    final static public String DONOR_ADD_PAGE = "donorData-add";
    final static public String DONOR_DATA_PAGE = "adminSuppliers";
    final static public String DONOR_VIEWDETAIL_PAGE = "donorData-detail";
    final static public String DONOR_DONORDETAIL_PAGE = "pub/donorDetails";

    enum FormMode {
        CREATE, EDIT
    }

    @EJB
    private DonorFacade donorFacade;
    private DonorEntity currentDonorData;
    private FormMode formMode;

    private UploadedFile photoFile;
    
    private Logger logger = Logger.getLogger("DonorDataBean");

    /**
     * 裝載表單資料的 Entity 物件
     */
    public FormMode getFormMode() {
        return formMode;
    }

    public void setFormMode(FormMode formMode) {
        this.formMode = formMode;
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public DonorFacade getDonorFacade() {
        return donorFacade;
    }

    public void setDonorFacade(DonorFacade donorFacade) {
        this.donorFacade = donorFacade;
    }

    public DonorEntity getCurrentDonorData() {
        return currentDonorData;
    }

    public void setCurrentDonorData(DonorEntity currentDonorData) {
        this.currentDonorData = currentDonorData;
    }

    public List<DonorEntity> getAllDonorData() {
        return donorFacade.findAll();
    }

    /**
     * @deprecated Use {@link #getCurrentDate() } to get the current date.
     * @return
     */
    public Date prepareCreateDate() {

        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public String saveDonor() {
        if (formMode == FormMode.CREATE) {
            donorFacade.create(currentDonorData);
        } else {
            currentDonorData.setCreate_date(getCurrentDate());
            donorFacade.edit(currentDonorData);
        }
        return DONOR_DATA_PAGE;
    }

    public String prepareCreate() {
        currentDonorData = new DonorEntity();
        currentDonorData.setCreate_date(getCurrentDate());
        formMode = FormMode.CREATE;
        return DONOR_ADD_PAGE;
    }

    public String prepareEdit(DonorEntity donor) {
        currentDonorData = donor;
        formMode = FormMode.EDIT;
        return DONOR_ADD_PAGE;
    }

    public void removeDonorData() {
        donorFacade.remove(currentDonorData);
    }

    public void removeDonorDataAction(DonorEntity donorEntity) {
        currentDonorData = donorEntity;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('deleteDialogVar').show();");
    }

    public String viewDonorDataAction(DonorEntity donorEntity) {
        currentDonorData = donorEntity;
        return DONOR_VIEWDETAIL_PAGE;
    }

    public List<DonorEntity> showcurrentdonor(int size) {
        List<DonorEntity> list = new ArrayList<DonorEntity>();
        for (int i = 0; i < size; i++) {
            list.add(currentDonorData);
        }
        return list;
    }

    public List<DonorEntity> findDonorsOrderByDate(int startDon, int rows) {
        return donorFacade.findDonorsOrderByCreate_Date(startDon, rows);
    }

    public void handleFileUpload(FileUploadEvent event) {
        photoFile = event.getFile();
        try {
            // convert from stream to byte array
            byte[] content = IOUtils.toByteArray(photoFile.getInputstream());
            currentDonorData.setPhoto(content);
        } catch (IOException ex) {
            Logger.getLogger(TicketDataBean.class.getName()).log(Level.SEVERE, "圖片上傳失敗", ex);
        }

    }

    public StreamedContent getDonorPhoto(DonorEntity donor) {
        if (donor.getPhoto() != null){
        String msg = "Donor image" + donor.getId();
        logger.info(msg);
        return new DefaultStreamedContent(new ByteArrayInputStream(donor.getPhoto()), "image/jpg");
        } else 
            return new DefaultStreamedContent();
    }

    /**
     * Return the ticket photo as the type of StreamedContent.
     * Use this function when you want to show image by using repeat tag.
     * @return
     */
    public StreamedContent getDonorPhoto() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            // The StreamedContent objects are placed in the session scope with an encrypted key.
            // After the rendering phase, the key is appended to image url that points to JSF resource handler.
            return new DefaultStreamedContent();
        } else {
            // Second request made by custom PrimeFaces ResourceHandler
            // So, browser is requesting the image. 
            // Convert the photo with byte array type to StreamedContent type.
            String donorID = facesContext.getExternalContext().getRequestParameterMap().get("donorID");
            DonorEntity donorEntity = donorFacade.find(Long.parseLong(donorID));
            logger.info("Donor to get image:" + donorEntity.getId().toString());
            return new DefaultStreamedContent(new ByteArrayInputStream(donorEntity.getPhoto()), "image/jpg");
        }
    }

    public String viewDonor(DonorEntity donor) {
        currentDonorData = donor;
        return DONOR_DONORDETAIL_PAGE;
    }
    
}
