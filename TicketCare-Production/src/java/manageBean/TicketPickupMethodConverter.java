/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import com.tcare.web.util.JsfUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import entity.TicketPickUpMethod;
import entity.TicketPickUpMethodFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author BoYenLai
 */
@FacesConverter("ticketPickupMethodConverter")
public class TicketPickupMethodConverter implements Converter {

    @EJB
    TicketPickUpMethodFacade ticketPickupMethodFacade;

    private Logger log = Logger.getLogger(TicketPickupMethodConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long pkValue = null;

        if (value == null) {
            String msg = "Please select a way from list.";
            String errMsg = generateErrorMsg("Null", msg);
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
            return null;
        }

        try {
            pkValue = Long.parseLong(value);

        } catch (NumberFormatException ex) {
            String msg = ". Cannot get a valid way id to find VolunteerTakeWayEntity instance.";
            String errMsg = generateErrorMsg(value, msg);
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
        }
        if (pkValue == null) {
            return null;
        }
        if (pkValue > 0) {
            return ticketPickupMethodFacade.find(pkValue);
        } else {
            return null;
        }
    }

    private String generateErrorMsg(String value, String msg) {
        StringBuilder msgbuilder = new StringBuilder();
        msgbuilder.append(TicketPickupMethodConverter.class.getName()).append(": Value: ");
        msgbuilder.append(value);
        msgbuilder.append(msg);
        return msgbuilder.toString();
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        TicketPickUpMethod currentMethod;
        if (value == null) {
            return null;
        }

        try {
            currentMethod = (TicketPickUpMethod) value;
        } catch (ClassCastException exception) {
            String errMsg = generateErrorMsg(value.toString(), ". Cannot cast to VolunteerTakeWayEntity instance.");
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
            return null;
        }
        // return the object ID.
        return currentMethod.getId().toString();
    }
}
