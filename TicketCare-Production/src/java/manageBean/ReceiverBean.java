package manageBean;

import entity.ReceiverFacade;
import entity.Receiver;
import entity.receiverDAO;
import javax.inject.Named;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 * 票卷接受者
 *
 * @author hychen39@gmail.com
 */
@Named(value = "receiver")
@SessionScoped
public class ReceiverBean implements Serializable {

    static public final String FILENAME = "Receivers.txt";
    final static public String RECEIVER_ADD_PAGE = "individualData-add";
    final static public String RECEIVER_DATA_PAGE = "adminIndividual";
    final static public String RECEIVER_VIEWDETAIL_PAGE = "individualData-detail";

    enum FormMode {
        CREATE, EDIT
    }

    private int birthYear;
    private int birthMonth;
    private int birthDay;
    private receiverDAO receiverDAO;
    private List<Receiver> filteredReceiver;

    @EJB
    private ReceiverFacade receiverDataFacade;
    private Receiver currentReceiver;
    private FormMode formMode;

    public ReceiverFacade getReceiverDataFacade() {
        return receiverDataFacade;
    }

    public void setReceiverDataFacade(ReceiverFacade receiverDataFacade) {
        this.receiverDataFacade = receiverDataFacade;
    }

    public Receiver getCurrentReceiver() {
        return currentReceiver;
    }

    public void setCurrentReceiver(Receiver currentReceiver) {
        this.currentReceiver = currentReceiver;
    }

    public FormMode getFormMode() {
        return formMode;
    }

    public void setFormMode(FormMode formMode) {
        this.formMode = formMode;
    }

    public List<Receiver> getFilteredReceiver() {
        return filteredReceiver;
    }

    public void setFilteredReceiver(List<Receiver> filteredReceiver) {
        this.filteredReceiver = filteredReceiver;
    }

    /**
     * Creates a new instance of Receiver
     */
    public ReceiverBean() {
    }

    @PostConstruct
    public void initBean() {
        //defaul the birth date
        setBirthYear(2016);
        // Month index start from 0
        setBirthMonth(1);
        // Day index start from 1
        setBirthDay(1);
        // create a new Receiver entity instance
        currentReceiver = new Receiver();
        List<String> interistrings = new ArrayList<>();
        currentReceiver.setInterestings(interistrings);
        currentReceiver.setInterestings(new ArrayList<String>());
        // init ReceiverDAO
        receiverDAO = new receiverDAO();
        receiverDAO.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/"));
        receiverDAO.setFilename(FILENAME);
    }

    public receiverDAO getReceiverDAO() {
        return receiverDAO;
    }

    public void setReceiverDAO(receiverDAO receiverDAO) {
        this.receiverDAO = receiverDAO;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }

    /**
     * 儲存 {@link #receiverEntity} 到檔案中。
     */
    public void save() {
        // 你沒有此目標 facelets
//        String targetPage = "/manage/cases/listAllReceivers";
//        String targetPage = "/individual";
        // create the year, month, and day fields to date
        StringBuilder birthDateBuilder = new StringBuilder();
        birthDateBuilder.append(getBirthYear()).append("/");
        birthDateBuilder.append(getBirthMonth()).append("/");
        birthDateBuilder.append(getBirthDay());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date birthDate = Calendar.getInstance().getTime();
        try {
            birthDate = sdf.parse(birthDateBuilder.toString());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverBean.class.getName()).log(Level.SEVERE, null, ex);
            // Throw error Message
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Errors occur when make the birth date."));
        }
        //Set the birth date property for the Receiver entity.
        currentReceiver.setBirth(birthDate);
        // call ReceiverDAO instance to save to file
        receiverDAO.save(currentReceiver);
        prepareAnotherCreate();
//        return targetPage;
    }

    /**
     * 為輸入下一筆新資料作準備動作。 先顯示對話框，表示儲存完成。之後，建立一個新的 {@link Receiver} 實體，供新的資料輸入。
     */
    private void prepareAnotherCreate() {
        // 顯示 MessageBox 訊息，表示儲存成功
        RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage("建檔成功"));
        currentReceiver = new Receiver();
    }

    /**
     * Get all receivers.
     *
     * @return
     */
    public List<Receiver> getReceivers() {
        List<Receiver> allList = null;
        allList = receiverDAO.getAll();
        return allList;
    }

    public List<Receiver> getAllIndividualData() {
        return receiverDataFacade.findAll();
    }

    /**
     * 建立新的 {@code Receiver} 實體，用以記錄使用者輸入的新資料。 Create a new {@code Receiver}
     * instance and assign to {@link ReceiverBean#receiverEntity}.
     *
     * @return 跳轉到輸入個案資料的頁面
     */
    public String prepareCreate() {
        //String targetPage = "individual-add";
        //receiverEntity = new Receiver();
        setBirthYear(1980);
        setBirthMonth(1);
        setBirthDay(1);
        //return targetPage;
        currentReceiver = new Receiver();
        formMode = FormMode.CREATE;
        return RECEIVER_ADD_PAGE;
    }

    public String saveIndividual() {

        try {
            currentReceiver.setBirth(convertBirthYearMonthDay());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (formMode == FormMode.CREATE) {
            receiverDataFacade.create(currentReceiver);
        } else {
            receiverDataFacade.edit(currentReceiver);
        }
        return RECEIVER_DATA_PAGE;
    }

    public String prepareEdit(Receiver receiver) {
        currentReceiver = receiver;
        formMode = FormMode.EDIT;
        return RECEIVER_ADD_PAGE;
    }

    public void removeindividualData() {
        receiverDataFacade.remove(currentReceiver);
        currentReceiver = null;
    }

    public void removeIndividualAction(Receiver receiver) {
        currentReceiver = receiver;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('deleteDialogVar').show();");
    }

    public String viewIndividualAction(Receiver receiver) {
        currentReceiver = receiver;
        return RECEIVER_VIEWDETAIL_PAGE;
    }

    public Date convertBirthYearMonthDay() throws ParseException {
        StringBuilder birthDateStrBuffer = new StringBuilder();
        birthDateStrBuffer.append(Integer.toString(getBirthYear())).append("/");
        birthDateStrBuffer.append(Integer.toString(getBirthMonth())).append("/");
        birthDateStrBuffer.append(Integer.toString(getBirthDay()));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        return dateFormat.parse(birthDateStrBuffer.toString());
    }
}
