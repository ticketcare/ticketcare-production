/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import entity.TicketPickUpMethodFacade;
import entity.TicketPickUpMethod;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;

@Named(value = "volunteerTakeWayBean")
@Dependent
public class VolunteerTakeWayBean {

    /**
     * Creates a new instance of volunteerTakeWayBean.
     */
    @EJB
    private TicketPickUpMethodFacade ticketPickupMethodFacade;

    public List<TicketPickUpMethod> getAllPickupMethod() {
        return ticketPickupMethodFacade.findAll();
    }

    @PostConstruct
    // EJB 建立後系統會呼叫此方法。
    public void init() {
        ticketPickupMethodFacade.populateContent();
    }

    public VolunteerTakeWayBean() {
    }

}
