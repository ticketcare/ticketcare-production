/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import static com.tcare.web.util.JsfUtil.getCurrentDate;
import entity.TicketReserveTxnFacade;
import entity.TicketingEntity;
import entity.TicketingFacade;
import entity.VolunteerFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import org.primefaces.context.RequestContext;

/**
 * JSF Managed Bean for {@code ticketRecords.xhtml}.
 * @author hychen39@gmail
 */
@Named(value = "ticketingBean")
@SessionScoped
public class TicketingBean implements Serializable {
    
    @EJB TicketReserveTxnFacade ticketReserveTxnFacade;
    @EJB TicketingFacade ticketingFacade;
    Logger logger = Logger.getLogger("ticketingBean");
    
    private TicketingEntity currentTicketing;
    
    /**
     * Creates a new instance of TicketingBean
     */
    public TicketingBean() {
    }
    

    public TicketingEntity getCurrentTicketing() {
        return currentTicketing;
    }

    public void setCurrentTicketing(TicketingEntity currentTicketing) {
        this.currentTicketing = currentTicketing;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('checkTicketTake').show();");
    }
    
    public void addpickupDateAction() {
        currentTicketing = new TicketingEntity();
    }
    
    /**
     * Generate the ticketing records from the ticket reservation transactions.
     */
    public void generateTicketingRecords(){
        List<TicketReserveTxnFacade.ReservTxnSummary> summary;
        summary = ticketReserveTxnFacade.findValidReservQty();
        int counts = ticketReserveTxnFacade.transfer2Ticketing(summary);
        ticketReserveTxnFacade.updateReserveTxnStatus(summary);
        // logging 
        String logStr = String.format("Transfer %d records.", counts);
        logger.info(logStr);
    }
    
    /**
     * Return all ticketing records
     * @return List of {@code TicketingEntity} type
     * @see TicketingEntity 
     * @see TicketingFacade
     */
    public List<TicketingEntity> findAllTicketingRecords(){
        List<TicketingEntity> records = ticketingFacade.findAll();
        return records;
    }
    
    /**
     * Do the pre-processing for loading the page
     * @return target page code: ticketingRecords
     */
    public String preparePage(){
        this.generateTicketingRecords();
        return "ticketingRecords";
    }
    
    /**
     * Get the current date.
     * @return 
     */
    public Date prepareCreateDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
    
    /**
     * 更新派票記錄的日期
     * @return null, 停留在目前頁面.
     */
    public String updateTicketingDate() {
        currentTicketing.setPickupDate(getCurrentDate());
        ticketingFacade.edit(currentTicketing);
        return null;
    }
    
}


