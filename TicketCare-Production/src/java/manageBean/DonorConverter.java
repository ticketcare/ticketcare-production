/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package manageBean;

import com.tcare.web.util.JsfUtil;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import entity.DonorFacade;
import entity.DonorEntity;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author hychen39@gmail.com
 */
@FacesConverter("DonorConverter")
public class DonorConverter implements Converter {

    @EJB
    DonorFacade donorFacade;

    private Logger log = Logger.getLogger(DonorConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Long pkValue = null;

        if (value == null) {
            String msg = "Please select a donor from list.";
            String errMsg = generateErrorMsg("Null", msg);
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
            return null;
        }

        try {
            pkValue = Long.parseLong(value);

        } catch (NumberFormatException ex) {
            String msg = ". Cannot get a valid category id to find Donor instance.";
            String errMsg = generateErrorMsg(value, msg);
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
        }
        if (pkValue == null) {
            return null;
        }
        if (pkValue > 0) {
            return donorFacade.find(pkValue);
        } else {
            return null;
        }
    }

    private String generateErrorMsg(String value, String msg) {
        StringBuilder msgbuilder = new StringBuilder();
        msgbuilder.append(DonorConverter.class.getName()).append(": Value: ");
        msgbuilder.append(value);
        msgbuilder.append(msg);
        return msgbuilder.toString();
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        DonorEntity currentDonor;
        if (value == null) {
            return null;
        }

        try {
            currentDonor = (DonorEntity) value;
        } catch (ClassCastException exception) {
            String errMsg = generateErrorMsg(value.toString(), ". Cannot cast to Donor instance.");
            log.log(Level.WARNING, errMsg);
            JsfUtil.addErrorMessage(errMsg);
            return null;
        }
        // return the object ID.
        return currentDonor.getId().toString();
    }
}
