package manageBean;

import entity.TicketCategoryEntity;
import entity.TicketCategoryFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.context.RequestContext;

@Named(value = "ticketCategoryBean")
@SessionScoped
public class TicketCategoryBean implements Serializable {

    /**
     * Inject the EJB 使用此 EJB 儲存及取得資料庫內的資料。
     */
    @EJB
    private TicketCategoryFacade ticketCategoryFacade;
    /**
     * 裝載表單資料的 Entity 物件
     */
    private TicketCategoryEntity currentCategory;

    public TicketCategoryEntity getCurrentCategory() {
        return currentCategory;
    }

    // Getters and Setters
    public TicketCategoryFacade getTicketCategoryFacade() {
        return ticketCategoryFacade;
    }

    public void setTicketCategoryFacade(TicketCategoryFacade ticketCategoryFacade) {
        this.ticketCategoryFacade = ticketCategoryFacade;
    }

    public List<TicketCategoryEntity> getAllCategories() {
        // 呼叫 ticketCategoryFacade 的 findAll 方法，去資料庫找出所有的資料。
        return ticketCategoryFacade.findAll();
    }

    /**
     * 在顯示表單供輸入資料前，先建立一個 Entity instance。
     */
    public void addCategoryAction() {
        currentCategory = new TicketCategoryEntity();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('addDialogVar').show();");
    }

    /**
     * 將 {@link #currentCategory} 存入資料庫。
     *
     * @return
     */
    public String addCategoryDB() {
        ticketCategoryFacade.create(currentCategory);
        // 準備另一個新物件來裝載下一個表單資料。
        currentCategory = new TicketCategoryEntity();
        return null;
    }

    public void updateCategoryDB() {
        ticketCategoryFacade.edit(currentCategory);
        currentCategory = null;
    }

    public void updateCategoryAction(TicketCategoryEntity entity) {
        currentCategory = entity;
        // pop up the pf dialog from server side
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('editDialogVar').show();");
    }

    public void removeCategoryDB() {
        ticketCategoryFacade.remove(currentCategory);
        currentCategory = null;
    }

    public void removeCategoryAction(TicketCategoryEntity entity) {
        currentCategory = entity;
        // pop up the pf dialog from server side
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('deleteDialogVar').show();");
    }

    public void CategoryCancel() {
        currentCategory = null;
    }
}
