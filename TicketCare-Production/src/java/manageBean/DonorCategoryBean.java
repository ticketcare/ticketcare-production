/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import entity.DonorCategoryFacade;
import entity.DonorCategoryEntity;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 *
 * @author user
 */
@Named(value = "donorCategoryBean")
@SessionScoped
public class DonorCategoryBean implements Serializable {

    /**
     * Creates a new instance of DonorCategoryBean.
     */
    @EJB
    private DonorCategoryFacade donorCategoryFacade;
    private DonorCategoryEntity currentDonorCategory;

    /* Don't need the setter and getter for DonorCategoryFacade because it is 
    * injected by the container.
     */
    public DonorCategoryEntity getCurrentDonorCategory() {
        return currentDonorCategory;
    }

    public List<DonorCategoryEntity> getAllCategories() {
        return donorCategoryFacade.findAll();
    }

    public DonorCategoryBean() {
    }

    @PostConstruct
    private void init() {
        donorCategoryFacade.populateContent();
    }
}
