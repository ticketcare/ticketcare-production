/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import entity.Ticket;
import entity.TicketDataFacade;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import manageBean.TicketDataBean.FormMode;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author steven
 */
@Named(value = "ticketDataBean")
@SessionScoped
public class TicketDataBean implements Serializable {

    final static public String TICKET_ADD_PAGE = "ticketData-add";
    final static public String TICKET_DATA_PAGE = "adminTicketData";
    final static public String TICKET_VIEWDETAIL_PAGE = "ticketData-detail";

    enum FormMode {
        CREATE, EDIT,
    }

    @EJB
    private TicketDataFacade ticketDataFacade;
    private Ticket currentTicketData;
    private FormMode formMode;
    static final transient private Logger ticketDataBeanLogger = Logger.getLogger("TicketDataBean");

    private UploadedFile photoFile;

    public FormMode getFormMode() {
        return formMode;
    }

    public void setFormMode(FormMode formMode) {
        this.formMode = formMode;
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public TicketDataFacade getTicketDataFacade() {
        return ticketDataFacade;
    }

    public void setTicketDataFacade(TicketDataFacade ticketDataFacade) {
        this.ticketDataFacade = ticketDataFacade;
    }

    public Ticket getCurrentTicketData() {
        return currentTicketData;
    }

    public void setCurrentTicketData(Ticket currentTicketData) {
        this.currentTicketData = currentTicketData;
    }

    public List<Ticket> getAllTickerData() {
        return ticketDataFacade.findAll();
    }

    /**
     * 請直接使用 {@link #getCurrentDate() }
     *
     * @deprecated 2017/3/23 by hychen39@gmail.com
     * @return
     */
    public Date prepareCreateDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    /**
     * Save the {@link #currentTicketData} to database by calling the
     * {@link #ticketDataFacade}.
     */
    public String saveTicket() {
        if (formMode == FormMode.CREATE) {
            ticketDataFacade.create(currentTicketData);
        } else {
            currentTicketData.setCreate_date(getCurrentDate());
            ticketDataFacade.edit(currentTicketData);
        }
        return TICKET_DATA_PAGE;
    }

    public String prepareCreate() {
        currentTicketData = new Ticket();
        currentTicketData.setCreate_date(getCurrentDate());
        formMode = FormMode.CREATE;
        return TICKET_ADD_PAGE;
    }

    public String prepareEdit(Ticket ticket) {
        currentTicketData = ticket;
        formMode = FormMode.EDIT;
        return TICKET_ADD_PAGE;
    }

    public void removeTicketData() {
        ticketDataFacade.remove(currentTicketData);
        currentTicketData = null;
    }

    public void removeTicketDataAction(Ticket ticket) {
        currentTicketData = ticket;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('deleteDialogVar').show();");
    }

    public String viewTicketDataAction(Ticket ticket) {
        currentTicketData = ticket;
        return TICKET_VIEWDETAIL_PAGE;
    }

    /**
     * Prepare for posting operation. Call PrimeFace dialog
     * <code>addNewPostDialogVar</code> to show up.
     *
     * @param ticket Ticket to post.
     */
    public void preparePosting(Ticket ticket) {
        currentTicketData = ticket;

        RequestContext.getCurrentInstance().execute("PF('addNewPostDialogVar').show();");

    }

    public void prepareOffPosting(Ticket ticket) {
        currentTicketData = ticket;
        RequestContext.getCurrentInstance().execute("PF('delPostDialogVar').show();");
    }

    public boolean isOnShelf(Ticket ticket) {
        return ticket.getStatus().equals(Ticket.Status.ON_SHELF);
    }

    public boolean isOffShelf(Ticket ticket) {
        return ticket.getStatus().equals(Ticket.Status.OFF_SHELF);
    }

    /**
     * Get the uploaded photo and put it to current ticket instance.
     *
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) {
        photoFile = event.getFile();
        try {
            // convert from stream to byte array
            byte[] content = IOUtils.toByteArray(photoFile.getInputstream());
            currentTicketData.setPhoto(content);
        } catch (IOException ex) {
            Logger.getLogger(TicketDataBean.class.getName()).log(Level.SEVERE, "圖片上傳失敗", ex);
        }
    }

    /**
     * Return the ticket photo as the type of StreamedContent.
     *
     * @param ticket
     * @return
     */
    public StreamedContent getTicketPhoto(Ticket ticket) {
        if (ticket.getPhoto() != null) {
            String msg = "Ticket to get image: " + ticket.getName();
            ticketDataBeanLogger.info(msg);
            return new DefaultStreamedContent(new ByteArrayInputStream(ticket.getPhoto()), "image/jpg");
        } else {
            return new DefaultStreamedContent();
        }
    }

    /**
     * Return the ticket photo as the type of StreamedContent.
     *
     * @return
     */
    public StreamedContent getTicketPhoto() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            // The StreamedContent objects are placed in the session scope with an encrypted key.
            // After the rendering phase, the key is appended to image url that points to JSF resource handler.
            return new DefaultStreamedContent();
        } else {
            // Second request made by custom PrimeFaces ResourceHandler
            // So, browser is requesting the image. 
            // Convert the photo with byte array type to StreamedContent type.
            String ticketID = facesContext.getExternalContext().getRequestParameterMap().get("ticketID");
            Ticket ticket = ticketDataFacade.find(Long.parseLong(ticketID));
            if (ticket.getPhoto() != null) {
                String msg = "Ticket to get image:" + ticket.getId().toString();
                ticketDataBeanLogger.info(msg);
                return new DefaultStreamedContent(new ByteArrayInputStream(ticket.getPhoto()), "image/jpg");
            } else {
                ticketDataBeanLogger.info(ticketID + " get null from database");
                return new DefaultStreamedContent();
            }
        }
    }
}
