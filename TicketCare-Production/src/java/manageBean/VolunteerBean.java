/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import entity.Volunteer;
import entity.VolunteerFacade;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.context.RequestContext;

/**
 *
 * @author BoYenLai
 */
@Named(value = "volunteerBean")
@SessionScoped
public class VolunteerBean implements Serializable {

    final static public String Volunteer_DATA_PAGE = "adminVolunteer";

    @EJB
    private VolunteerFacade volunteerFacade;

    private Volunteer currentVolunteer;

    public VolunteerFacade getVolunteerFacade() {
        return volunteerFacade;
    }

    public void setVolunteerFacade(VolunteerFacade volunteerFacade) {
        this.volunteerFacade = volunteerFacade;
    }

    public Volunteer getCurrentVolunteer() {
        return currentVolunteer;
    }

    public void setCurrentVolunteer(Volunteer currentVolunteer) {
        this.currentVolunteer = currentVolunteer;
    }

    public List<Volunteer> getAllVolunteers() {
        List<Volunteer> volunteers = null;
        return volunteerFacade.findAll();
    }

    public void resetVolunteerPasswordAction(Volunteer entity) {
        currentVolunteer = entity;
        // pop up the pf dialog from server side
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('resetVolunteerPasswordDialogVar').show();");
    }

    public String resetVolunteerPasswordDB() {
        currentVolunteer.setPassword("0000");
        volunteerFacade.edit(currentVolunteer);
        return null;
    }

    public void VolunteerPasswordCancel() {
        currentVolunteer = null;
    }

    public void removeVolunteerAction(Volunteer entity) {
        currentVolunteer = entity;
        // pop up the pf dialog from server side
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('deleteDialogVar').show();");
    }

    public void removeVolunteerDB() {
        volunteerFacade.remove(currentVolunteer);
        currentVolunteer = null;
    }

    /**
     * Creates a new instance of VolunteerBean
     */
    public VolunteerBean() {
    }

}
