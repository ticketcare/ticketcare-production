/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manageBean;

import entity.DispatchEntity;
import entity.DispatchFacade;
import entity.TicketReserveTxn;
import entity.TicketReserveTxnFacade;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author user
 */
@Named(value = "dispatchBean")
@Stateless
@ViewScoped
public class DispatchBean implements Serializable {

    private DispatchEntity currentDispatch;

    @EJB
    private DispatchFacade dispatchFacade;
    @EJB
    private TicketReserveTxnFacade ticketResvFacade;
    private TicketReserveTxn ticketTxn;

    public DispatchEntity getCurrentDispatch() {
        return currentDispatch;
    }

    public void setCurrentDispatch(DispatchEntity currentDispatch) {
        this.currentDispatch = currentDispatch;
    }

    public DispatchFacade getDispatchFacade() {
        return dispatchFacade;
    }

    public void setDispatchFacade(DispatchFacade dispatchFacade) {
        this.dispatchFacade = dispatchFacade;
    }

    public Date prepareCreateDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    /**
     * 在顯示表單供輸入資料前，先建立一個 Entity instance。
     */
    public void addDispatchAction() {
        currentDispatch = new DispatchEntity();
    }

    /**
     * 將 {@link #currentCategory} 存入資料庫。
     *
     * @return
     */
    public String addDispatchDB() {

        currentDispatch.setTxn_execute_timestamp(getCurrentDate());
        dispatchFacade.create(currentDispatch);
        // 準備另一個新物件來裝載下一個表單資料。
        currentDispatch = new DispatchEntity();
        return null;
    }

    public void DispatchCancel() {
        currentDispatch = null;
    }

    public void saveTicketTxn() {
        dispatchFacade.create(currentDispatch);
    }

}
