package manageBean.individuals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.model.SelectItem;

/**
 * 提供出生日期的月份及日數選項
 *
 * @author hychen39@gmail.com
 */
@Named(value = "birthDateBean")
@Dependent
public class BirthDateBean {

    List<SelectItem> monthItems;
    Calendar calendar;
    Locale defaultLocale;

    /**
     * Creates a new instance of birthDateBean
     */
    public BirthDateBean() {
    }

    @PostConstruct
    public void init() {

        monthItems = new ArrayList<>();
//        defaultLocale = Locale.getDefault();
        defaultLocale = Locale.TRADITIONAL_CHINESE;
        calendar = new GregorianCalendar(defaultLocale);
        SelectItem[] items = new SelectItem[12];
        Map<String, Integer> months = calendar.getDisplayNames(Calendar.MONTH, Calendar.LONG, defaultLocale);
        TreeMap<String, Integer> sortedMonths = new TreeMap<>(months);
        // Prepare month display name;
        for (String monthStrKey : sortedMonths.keySet()) {
            Integer monthValue = sortedMonths.get(monthStrKey);
            items[monthValue] = new SelectItem(monthValue + 1, monthStrKey);
        }
        monthItems = Arrays.asList(items);
    }

    public List<SelectItem> getMonthItems() {
        return monthItems;
    }

    /**
     * Return the maximum days in a given month and year.
     *
     * @param year
     * @param monthCode constant defined in the {@code java.util.Calendar}
     * @return
     */
    public List<SelectItem> getDayItems(int year, int monthCode) {
        List<SelectItem> dayItems = null;
        //set calendar to the first day of the given month and year
        calendar.set(year, monthCode - 1, 1);
        // Find the actual maximum day 
        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        // Create the available select items
        dayItems = new ArrayList<>();
        for (int i = 1; i <= maxDays; i++) {
            dayItems.add(new SelectItem(i, i + ""));
        }
        return dayItems;
    }

    public int getCurrentYear() {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear;
    }
}
