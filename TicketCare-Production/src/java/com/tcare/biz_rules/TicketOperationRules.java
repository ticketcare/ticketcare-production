/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.biz_rules;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * 票務作業的商業規則
 *
 * @author hychen39@gmail.com
 */
public class TicketOperationRules {

    static public Date ticketPickupDueDate() {
        int allowance_days = 3;
        Date dueDate = null;
        Instant newInstant = Instant.now().plus(allowance_days, ChronoUnit.DAYS);
        dueDate = Date.from(newInstant);
        return dueDate;
    }
}
