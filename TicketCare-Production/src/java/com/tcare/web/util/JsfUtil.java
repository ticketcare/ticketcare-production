package com.tcare.web.util;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

/**
 * Jsf utility object. Provide the following operations:
 * <ul>
 * <li>Add messages for {@code FacesContext}: Provide methods like
 * {@link #addErrorMessage(java.lang.String)} to add messages to
 * {@code FacesContext}. Use {@link #addSuccessMessage(java.lang.String)
 * to add the successful messages.</li>
 * <li> Get values from the request parameters: Use {@link #getRequestParameter(java.lang.String) } to
 * get the string value of the request parameter. Use {@link #getObjectFromRequestParameter(java.lang.String, javax.faces.convert.Converter, javax.faces.component.UIComponent) }
 * to get and convert the value of the request parameter to an object.
 * </li>
 *
 * <li>
 * Convert a collection entity to an array of {@code SelectItem}: Use {@link #getSelectItems(java.util.List, boolean) }
 * to make this operation.
 * </li>
 * <li>
 * Check if the validation fails: Use {@link #isValidationFailed() } to check if the validation fails.
 * </li>
 * </ul> @author hychen39@gmail.com
 */
public class JsfUtil {

    /**
     * Generate an array of {@code SelectItem} for {@code h:selectItems} tag.
     *
     * @param entities values for the {@code h:selectItems} tag.
     * @param selectOne whether or not add a select item for prompting users to
     * select one. The prompting select item has empty string value and a label
     * with "---".
     * @return {@code SelectItem} array.
     */
    public static SelectItem[] getSelectItems(List<? extends SelectItemLabelValue> entities, boolean selectOne) {
        int size = selectOne ? entities.size() + 1 : entities.size();
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        if (selectOne) {
            items[0] = new SelectItem("", "---");
            i++;
        }
        for (SelectItemLabelValue x : entities) {
            items[i++] = new SelectItem(x.getValue(), x.getLabel());
        }
        return items;
    }

    public static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }

    /**
     * Add the error message in the {@code Exception} to FacesContext
     *
     * @param ex
     * @param defaultMsg
     */
    public static void addErrorMessage(Exception ex, String defaultMsg) {
        String msg = ex.getLocalizedMessage();
        if (msg != null && msg.length() > 0) {
            addErrorMessage(msg);
        } else {
            addErrorMessage(defaultMsg);
        }
    }

    public static void addErrorMessages(List<String> messages) {
        for (String message : messages) {
            addErrorMessage(message);
        }
    }

    /**
     * Add general error message to FacesContext.
     *
     * @param msg error message to add.
     */
    public static void addErrorMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addErrorMessage(String cliendId, String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
        FacesContext.getCurrentInstance().addMessage(cliendId, facesMsg);
    }

    public static void addSuccessMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
    }

    public static String getRequestParameter(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }

    public static Object getObjectFromRequestParameter(String requestParameterName, Converter converter, UIComponent component) {
        String theId = JsfUtil.getRequestParameter(requestParameterName);
        return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
    }

    public static enum PersistAction {
        CREATE,
        DELETE,
        UPDATE
    }

    /**
     * Get current date using Java 8 API
     *
     * @return
     */
    public static Date getCurrentDate() {
        Instant now = Instant.now();
        return Date.from(now);
    }

}
