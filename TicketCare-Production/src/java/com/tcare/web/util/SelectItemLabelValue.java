/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.web.util;

/**
 * operations to get the labels and values for a list of objects to be used as
 * select items.
 *
 * @author hychen39@gmail.com
 */
public interface SelectItemLabelValue {

    String getLabel();

    String getValue();
}
